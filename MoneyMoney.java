import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MoneyMoney {


    public static void main(String[] args) {
        List<Integer> expenses = new ArrayList<>();
        List<Integer> expensesInfo = expensesInformation(expenses);
    
        // System.out.print("Salary: "+  expensesInfo.get(0) +'\n');
        // System.out.print("Rent: "+  expensesInfo.get(1) +'\n');
        // System.out.print("Bills: "+  expensesInfo.get(2) +'\n');
        // System.out.print("Treats: "+  expensesInfo.get(3) +'\n');
        // System.out.print("Food: "+  expensesInfo.get(4) +'\n');

        List<Float> percentages = new ArrayList<>();
        List<Float> percentInfo = calculations(expensesInfo, percentages);
        System.out.println();

        // System.out.print("Rent Percent: "+  percentInfo.get(0) +'\n');
        // System.out.print("Bill Percent: "+  percentInfo.get(1) +'\n');
        // System.out.print("Treat Percent: "+  percentInfo.get(2) +'\n');
        // System.out.print("Food Percent: "+  percentInfo.get(3) +'\n');

        double taxInfo = taxes(expenses);
        // System.out.println(taxInfo);

        printTable(expensesInfo, taxInfo, percentInfo);
    
       


    }

    //Asks user about how much they spend a month.
    public static List<Integer> expensesInformation(List<Integer> expenses) {
        System.out.println("Greetings!");
        System.out.println("Welcome to: WHERE'S YOUR MONEY GOING?");
        
        Scanner scanner = new Scanner(System.in); //creates new scanner
        System.out.println("How much money do you make annually?");
        int salary = 30000;
        // int salary = scanner.nextInt();
        
        System.out.println("What is your monthly mortgage/rent bill?");
        // int rent = scanner.nextInt();
        int rent = 400;

        System.out.println("Include your bills such as electricity, wi-fi, utilities, etc?");
        // int bills = scanner.nextInt();
        int bills = 700;

        System.out.println("How much do you spend on treating yourself?");
        System.out.println("Hair, nails clothes, etc.?");
        // int treats = scanner.nextInt();
        int treats = 100;

        System.out.println("And finally, how much do you spend on groceries and food?");
        // int food = scanner.nextInt();
        int food = 300;

        expenses.add(salary);
        expenses.add(rent);
        expenses.add(bills);
        expenses.add(treats);
        expenses.add(food);
    
        scanner.close();
        return expenses;

        }

    //Implement formatting of numbers method
    public static List<Float> calculations(List<Integer> expensesInfo, List<Float> percentages) {
           
            float salary = expensesInfo.get(0);
            float rent = expensesInfo.get(1);
            float bills = expensesInfo.get(2);
            float treats = expensesInfo.get(3);
            float food = expensesInfo.get(4);

            float rentPercent = (rent*12/salary)*100;
            float billPercent = (bills*12/salary)*100;
            float treatPercent = (treats*12/salary)*100;
            float foodPercent = (food*12/salary)*100;

            percentages.add(rentPercent);
            percentages.add(billPercent);
            percentages.add(treatPercent);
            percentages.add(foodPercent);

            return percentages;
    }

    //Sets different tax rates based on their salary
    public static double taxes(List<Integer> expensesInfo) {
        var salary = expensesInfo.get(0);
        double tax = 0.0;
        if (salary < 15000) {
            tax = 0.10;
        }else if(15000 < salary || salary <= 75000) {
            tax = 0.20;
        }else if(75000 < salary || salary <= 200000) {
            tax = 0.25;
        }else if(salary > 200000) {
            tax = 0.30;
        }
        return tax; 
    }


    //Display results in a table format.
    public static void printTable(List<Integer> expensesInfo, double taxInfo, List<Float> percentInfo){
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("See the financial breakdown below based on the salary of: $"+expensesInfo.get(0));
        System.out.println("-------------------------------------------------------------------------");

        // for(int i = 0; i < expensesInfo.length; i++) {
        //     System.out.print()
        // }

        System.out.print("| Mortgage/Rent  |"+" $" + expensesInfo.get(1) + percentInfo.get(0) +"\n");
        System.out.print("| Bills          |"+" $" + expensesInfo.get(2) + percentInfo.get(1) +"\n");
        System.out.print("| Treats         |"+" $" + expensesInfo.get(3) + percentInfo.get(2) +"\n");
        System.out.print("| Food/Groceries |"+" $" + expensesInfo.get(4) + percentInfo.get(3) +"\n");
        System.out.print("| Taxes          |"+" $" + taxInfo +"\n");

        System.out.println("\n-------------------------------------------------------------------------");
    }

//add more code here


}
